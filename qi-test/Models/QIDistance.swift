//
//  QIDistance.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/22/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import ObjectMapper


class QIDistanceResponse: Mappable {

    var rows: [QIRow]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        rows   <- map["rows"]
    }
    
}

class QIRow: Mappable {
    var elements: [QIElement]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        elements   <- map["elements"]
    }
    
}

class QIElement: Mappable {
    var distanceValue: Double?
    var distanceText:  String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        distanceValue   <- map["distance.value"]
        distanceText    <- map["distance.text"]
    }
    
}


class QIDistance: Mappable {
    var nextPageToken: String?
    var locations: [QILocation]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        nextPageToken   <- map["next_page_token"]
        locations       <- map["results"]
    }

}
