//
//  QICategory.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/23/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class QICategory: NSObject,NSCoding {
    var name:String
    var counter:NSNumber
    
    init(withName categoryName:String) {
       
        name = categoryName
        counter = 0
        super.init()
    }
    
    func increaseCounter()->NSNumber{
        self.counter = NSNumber(integerLiteral: self.counter .intValue + 1)
        return self.counter
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.counter = decoder.decodeObject(forKey: "counter") as? NSNumber ?? 0
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name, forKey: "name")
        aCoder.encode(counter, forKey: "counter")
    }
    
    override var description: String{
        return "Name - \(name) | Counter - \(counter) \n"
    }

}
