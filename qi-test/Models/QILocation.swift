//
//  QILocation.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import ObjectMapper

class QILocationResponse: Mappable{
    var nextPageToken: String?
    var locations: [QILocation]?
    
 
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        nextPageToken   <- map["next_page_token"]
        locations       <- map["results"]
    }
}

class QIimage:Mappable{
    var photoReference: String?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        photoReference   <- map["photo_reference"]
    }
}

class QILocation: Mappable {
    
    var name: String?
    var types: [String]?
    var isOpen: Bool?
    var photos: [QIimage]?
    var vicinity: String?
    var lat: Double?
    var long: Double?
    var imageURL:URL?{
        get{
            
            let photoReference = photos?[0].photoReference ?? ""
            
            let urlString = QIConstants.IMAGE_BASE_URL+"?photoreference="+photoReference+"&key="+QIConstants.API_KEY+"&maxwidth=200"

            return URL(string: urlString)
        }
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name            <- map["name"]
        types           <- map["types"]
        isOpen          <- map["opening_hours.open_now"]
        photos          <- map["photos"]
        vicinity        <- map["vicinity"]
        lat             <- map["geometry.location.lat"]
        long            <- map["geometry.location.lng"]
    }
    
    
    
}



