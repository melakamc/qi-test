//
//  QIConstants.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

public struct QIConstants {
    
    static let API_BASE_URL = "https://maps.googleapis.com/maps/api/"
    static let IMAGE_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo"
    static let DISTANCE_BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/"
    static let API_KEY = "AIzaSyCdTkUa3knqgp78gN_S65c65l8TTYDloNA"

}
