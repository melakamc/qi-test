//
//  QIMapViewController.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/22/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SwiftSpinner

class QIMapViewController: UIViewController {
    
    var locations:[QILocation] = [QILocation]()
    var currentIndex = 0
    var currentLocation = CLLocation()
    var locationMarker = GMSMarker()
    
    @IBOutlet weak var locationVicinity: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var openStatus: UILabel!
 
    @IBOutlet var leftSwipeGestureRecognizer: UISwipeGestureRecognizer!
    @IBOutlet var rightSwipeGestureRecognizer: UISwipeGestureRecognizer!


    @IBAction func swipedRight(_ sender: Any) {
        //going back
        if(currentIndex > 0){
            currentIndex -= 1
            setUpUIForLocation(location: locations[currentIndex])
        }
    }
    @IBAction func swipedLeft(_ sender: Any) {
        //going front
        if(currentIndex < (locations.count - 1)){
            currentIndex += 1
            setUpUIForLocation(location: locations[currentIndex])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 12.0)
        mapView.camera = camera
    
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude , longitude: currentLocation.coordinate.longitude)
        marker.title = "Current Location"
        marker.map = mapView
        
        
        setUpUIForLocation(location: locations[currentIndex])
    }
    
    
    func setUpUIForLocation(location:QILocation)  {
        SwiftSpinner.show("Loading..")
        
        let toLocation = CLLocation(latitude: location.lat!, longitude: location.long!)
        
        QIDataFetcher.getDistance(fromLocation: currentLocation, toLocation: toLocation) {[unowned self] (distance, error) in
            SwiftSpinner.hide()
            self.distance.text = distance ?? "NA"
        }
        locationName.text = location.name
        locationVicinity.text = location.vicinity
        if let imageURL = location.imageURL{
            locationImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "no_image"))
        }
        if let isOpen  = location.isOpen{
            if isOpen{
                openStatus.text = "Open"
            }else{
                openStatus.text = "Closed"
            }
            
        }else{
            
            openStatus.text = "NA"
        }
        
       
        locationMarker.position = CLLocationCoordinate2D(latitude: location.lat ?? 0.0, longitude: location.long ?? 0.0)
        locationMarker.title = location.name
        locationMarker.map = mapView
    }


}
