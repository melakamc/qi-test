//
//  QIHomeTableViewController.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
private let reuseIdentifier = "CetegoryCell"

class QIHomeTableViewController: UITableViewController {
    
    var categories = [QICategory]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Pick a category"
        categories = QILocalDataManager.getCategories()
        self.tableView.reloadData()
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        QILocalDataManager.saveUpdatedCategoryList(categories)
    }
}

//MARK - TableView Delegate Methods
extension QIHomeTableViewController{
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier:reuseIdentifier)
        
        cell?.textLabel?.text = self.categories[indexPath.row].name
        
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:QILocationCollectionViewController = storyboard.instantiateViewController(withIdentifier: "QILocationCollectionViewController") as! QILocationCollectionViewController
        let category = self.categories[indexPath.row]
        _ = category.increaseCounter()
        vc.category = category.name
        self.navigationController?.pushViewController(vc, animated: true)
        
       
    }
}


// MARK: - Table view data source
extension QIHomeTableViewController{
   

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count;
    }

}
