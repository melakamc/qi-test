//
//  QILocationCollectionViewController.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import GoogleMaps
import SwiftSpinner
import SwiftMessages
private let reuseIdentifier = "LocationCell"


class QILocationCollectionViewController: UICollectionViewController{
    var category = ""
    var locations:[QILocation] = [QILocation]()
    var currentLocation = CLLocation()
    var locManager = CLLocationManager()
    var nextPageToken:String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        if let location = getCurrentLocation(){
            currentLocation = location
            self.fetchLocations(forCategory: category, lat: location.coordinate.latitude, lng: location.coordinate.longitude)
        }else{
            print("Couldn't fetch location")
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locManager.requestWhenInUseAuthorization()
       
    }
 

    
    


}


//MARK - CollectionView Layout

extension QILocationCollectionViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.size.width / 2) - 4)
        let height = width * 1.3
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}


//MARK - CollectionView Datasource
extension QILocationCollectionViewController{
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return locations.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! QILocationCollectionViewCell
        
        cell.locationName.text = self.locations[indexPath.row].name
        cell.vicinity.text = self.locations[indexPath.row].vicinity
        if let isOpen  = self.locations[indexPath.row].isOpen{
            if isOpen{
                cell.openStatus.text = "Open"
            }else{
                cell.openStatus.text = "Closed"
            }
            
        }else{
            
            cell.openStatus.text = "NA"
        }
        cell.types.text =  locations[indexPath.row].types?.joined(separator: ",")
        if let imageUrl = locations[indexPath.row].imageURL{
            cell.locationImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "no_image"))
        }
 
        return cell
    }
}

//MARK - CollectionView Delegate
extension QILocationCollectionViewController{
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QIMapViewController") as! QIMapViewController
        vc.locations = self.locations
        vc.currentIndex = indexPath.row
        vc.currentLocation = currentLocation
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(locations.count - indexPath.row == 6 ){
            fetchLocations(forCategory: category, lat: currentLocation.coordinate.latitude, lng: currentLocation.coordinate.longitude)
        }
    }
}

//MARK - Data fetching logic
extension QILocationCollectionViewController{
    func fetchLocations(forCategory:String, lat:Double, lng:Double)  {
        
      
        SwiftSpinner.show("Loading nearby locations..")
    
        
        QIDataFetcher.fetchNearByLocations(type: category,lat: lat, lng: lng, pageToken: nextPageToken) { [unowned self] (locations, pageToken, error) in
          
            
            SwiftSpinner.hide()
         
            
            if (error != nil) {
                print(error!.localizedDescription)
                return
            }
            self.nextPageToken = pageToken
            self.locations = self.locations  + locations!
            self.collectionView?.reloadData()
        }
    }
}

//MARK - Location fetching logic
extension QILocationCollectionViewController{
    
    func getCurrentLocation() -> CLLocation? {

        
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            return locManager.location
            
        }else{
            return nil
        }
        
    }
}
