//
//  QIRouter.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

public enum QIRouter: URLRequestConvertible {
    
    
    case locations(type:String,lat:Double, lng:Double, pageToken:String?)
    case distance(fromLocation:CLLocation, toLocation:CLLocation)
    
    var method: HTTPMethod {
        switch self {
        case .locations, .distance:
            return .get
        }
    }
    
    var path: String {
        switch self {
            
        case .locations(_ ):
            return "place/nearbysearch/json"
            
        case .distance(_ ):
            return "distancematrix/json"
        }
        
    }
    
    
    public func asURLRequest() throws -> URLRequest {
        let parameters: [String: Any] = {
            switch self {
            case .locations(let type, let lat, let lng, let pageToken):
                return ["key":QIConstants.API_KEY,
                        "type":type, "keyword":type,
                        "radius":"5000",
                        "location":"\(lat),\(lng)",
                        "pagetoken":pageToken ?? ""]
                
            case .distance (let fromLocation, let toLocation):
                let from = "\(fromLocation.coordinate.latitude),\(fromLocation.coordinate.longitude)"
                let to = "\(toLocation.coordinate.latitude),\(toLocation.coordinate.longitude)"
                return ["key":QIConstants.API_KEY,
                        "origins":from,
                        "destinations":to,
                        "units":"metric"]
            }
        }()
        
        let url = try QIConstants.API_BASE_URL.asURL()
        
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.timeoutInterval = TimeInterval(10 * 1000)
   
        return try URLEncoding.default.encode(request, with: parameters)
    }
}
