//
//  QIDataFetcher.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import AlamofireObjectMapper

public class QIDataFetcher: NSObject {
    
    static func fetchNearByLocations(type:String,lat:Double, lng:Double, pageToken:String?, withCallback callback:@escaping (_ locations:[QILocation]?,_ pageToken:String?,_ error:Error?)-> ())->Void{
        var url:URLRequest? = nil;
        do{
            
             url =  try QIRouter.locations(type: type,lat: lat, lng: lng, pageToken: pageToken).asURLRequest() ;
        }catch{
            
        }
        
        print(url?.url)
        
        Alamofire.request(QIRouter.locations(type: type,lat: lat, lng: lng, pageToken: pageToken)).responseObject { (response: DataResponse<QILocationResponse>) in
            if let error = response.error{
                callback(nil,nil,error)
                return;
            }
            let locationsResponse = response.result.value
            
            //print(locationsResponse)
            
            if let locations = locationsResponse?.locations{
                
                callback(locations, locationsResponse?.nextPageToken ,nil)
            }
           
        }
    }
    
    static func getDistance(fromLocation:CLLocation, toLocation:CLLocation ,withCallback callback:@escaping (_ distance:String?,_ error:Error?)-> ())->Void{
        var url:URLRequest? = nil;
        do{
            
            url =  try QIRouter.distance(fromLocation:fromLocation, toLocation: toLocation).asURLRequest() ;
        }catch{
            
        }
        
        print(url?.url)
        
        Alamofire.request(QIRouter.distance(fromLocation:fromLocation, toLocation: toLocation)).responseObject { (response: DataResponse<QIDistanceResponse>) in
            if let error = response.error{
                callback(nil,error)
                return;
            }
            
            let distanceResponse = response.result.value
            
            if let distance = distanceResponse?.rows?[0].elements?[0].distanceText{
                
                callback(distance,nil)
            }else{
                
                callback("NA",nil)
            }
            
        }
        
        
    }
    
}
