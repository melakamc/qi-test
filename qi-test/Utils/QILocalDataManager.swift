//
//  QILocationManager.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class QILocalDataManager: NSObject {
    
    public static func getCategories()->[QICategory]{
        
        
        if let data = UserDefaults.standard.object(forKey: "qi-categories") {
            
            let categoryList = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! [QICategory]
            let sortedCategories = categoryList.sorted(by:{ $0.counter.intValue > $1.counter.intValue })
            return sortedCategories
        }
        let categories = [
            QICategory.init(withName: "Bakery"),
            QICategory.init(withName: "Doctor"),
            QICategory.init(withName: "School"),
            QICategory.init(withName: "Taxi Stand"),
            QICategory.init(withName: "Hair care"),
            QICategory.init(withName: "Restaurant"),
            QICategory.init(withName: "Pharmacy"),
            QICategory.init(withName: "Atm"),
            QICategory.init(withName: "Store"),
            QICategory.init(withName: "Spa")
            
        ]
        
        return categories
     
        
    }
    
    public static func saveUpdatedCategoryList(_ list:[QICategory]){
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: list)
        UserDefaults.standard.set(encodedData, forKey: "qi-categories")
        UserDefaults.standard.synchronize()
    }
    
}
