//
//  QILocationCollectionViewCell.swift
//  qi-test
//
//  Created by Melaka Atalugamage on 6/20/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class QILocationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var vicinity: UILabel!
    @IBOutlet weak var types: UILabel!
    @IBOutlet weak var openStatus: UILabel!
    
}
